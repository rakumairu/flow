import csv, datetime

def read_csv(file_path):
    """"
    Read csv file as Python dictionary
    """
    # Initialize dictionary
    log = dict()

    # Read csv file
    with open(file_path, newline='') as csv_file:
        # Initialize csv reader
        f = csv.reader(csv_file, delimiter=',', quotechar='|')
        # Get next value
        next(f)
        for line in f:
            # Get case id, task and timestamp value
            case_id = line[0]
            task = line[1]
            timestamp = line[2]

            # Initialize log for each case_id
            if case_id not in log:
                log[case_id] = []

            event = (task, timestamp)
            log[case_id].append(event)
    
    # Return dictionary
    return log