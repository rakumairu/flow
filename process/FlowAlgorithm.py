def flow(eLog):
    """"
    Flow Algorithm to count transaction that exist in the process
    """
    # Initialize dictionary
    F = dict()

    # For each case_id in event_log
    for case_id in eLog:
        # For each transition in event_log
        for i in range(0, len(eLog[case_id])-1): #-1 so it won't return outofindex error
            # Current case_id
            a1 = eLog[case_id][i][0]
            # Next case_id
            a2 = eLog[case_id][i+1][0]

            # Initialize the second dimension of the dictionary
            if a1 not in F:
                F[a1] = dict()
            if a2 not in F[a1]:
                F[a1][a2] = 0

            F[a1][a2] += 1
    
    # Return the dictionary
    return F