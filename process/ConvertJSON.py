import csv, json
import pandas as pd

def cv_json(csv_file, json_file):
    # Load Data
    data = pd.read_csv(csv_file)

    # Mengambil header dari file
    head = list(data.columns.values)

    # Inisialisasi data untuk json
    dataset = []

    # TODO: handle kalo data NaN di ganti jadi none atau sejenisnya

    for el in data.values:
        row = {}
        for e in range(0, len(el)):
            row[head[e]] = el[e]
        dataset.append(row)
    
    # Load array menjadi json
    # myjson = json.loads(dataset)
    myjson = dataset

    # Save json to file
    with open(json_file, 'w') as jfile:
        json.dump(myjson, jfile)
    
    # Return json object
    return myjson    
