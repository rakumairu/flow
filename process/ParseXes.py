import datetime
import csv
import xml.etree.ElementTree as ET


def parse_xes(xes_file):
    """"
    Prase xes file to csv file
    """
    # Parse the xes to object
    tree = ET.parse(xes_file)
    # Get the root of the object
    root = tree.getroot()

    # Initialize the list of data
    data_list = []

    # Declare namespace for xes
    ns = {'xes': 'http://www.xes-standard.org/'}

    for trace in root.findall('xes:trace', ns):
        case_id = ''
        for string in trace.findall('xes:string', ns):
            if string.attrib['key'] == 'concept:name':
                case_id = string.attrib['value']
        for event in trace.findall('xes:event', ns):
            task = ''
            user = ''
            event_type = ''
            for string in event.findall('xes:string', ns):
                if string.attrib['key'] == 'concept:name':
                    task = string.attrib['value']
                if string.attrib['key'] == 'org:resource':
                    user = string.attrib['value']
                if string.attrib['key'] == 'lifecycle:transition':
                    event_type = string.attrib['value']
            timestamp = ''
            for date in event.findall('xes:date', ns):
                if date.attrib['key'] == 'time:timestamp':
                    timestamp = date.attrib['value']
                    timestamp = datetime.datetime.strptime(timestamp[:-10], '%Y-%m-%dT%H:%M:%S')

            temp = {'case_id': case_id, 'task': task, 'event_type': event_type, 'user': user, 'timestamp': str(timestamp)}
            print(temp)
            data_list.append(temp)

    # Save the file to raw.csv
    with open('./static/data/raw.csv', 'w') as file:
        header = ['case_id', 'task', 'event_type', 'user', 'timestamp']
        writer = csv.DictWriter(file, header)

        writer.writeheader()
        for line in data_list:
            writer.writerow(line)