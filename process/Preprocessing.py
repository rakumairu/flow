import pandas as pd

def preproccess(col_case_id, col_task, col_timestamp, mis_val):
    """"
    Preprocessing raw data
    """
    # Declare file path
    raw_file = './static/data/raw.csv'
    final_file = './static/data/final.csv'
    
    # Load data
    df = pd.read_csv(raw_file)

    # Select column and rename column
    case_id = df.iloc[:, [col_case_id]]
    task = df.iloc[:, [col_task]]
    timestamp = df.iloc[:, [col_timestamp]]

    # Join the column
    df = pd.concat([case_id, task, timestamp], axis=1, sort=False)

    # Rename the column
    df.columns = ['case_id','task','timestamp']

    # Save file to csv format
    df.to_csv(final_file, index=False)