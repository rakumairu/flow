from flask import Flask, render_template, redirect, request
from process.ParseXes import parse_xes
from process.Preprocessing import preproccess
from process.ReadCSV import read_csv
from process.FlowAlgorithm import flow
from process.Filtering import filters
from process.ConvertJSON import cv_json
import os, json, csv


app = Flask(__name__)

# Declare file path
raw_file = './static/data/raw.csv'
final_file = './static/data/final.csv'
raw_json = './static/data/raw.json'
final_json = './static/data/final.json'
path_data = './static/data'
xes_file = './static/data/raw.xes'


@app.route('/')
def index():
    """
    Index route for Flow
    """
    # If data already exist redirect upload
    if os.path.isfile(raw_file):
        return redirect('display', code=302)

    # Render index.html
    return render_template('index.html', title='Upload')

@app.route('/upload', methods=['POST'])
def upload():
    """
    Upload data
    """
    # Fetch file from user
    file = request.files['file']

    # Check file extension, xes/csv
    # If extension is xes
    if file.filename.endswith('.xes'):
        file.save(os.path.join(path_data, 'raw.xes'))
        # Parse file to csv format
        parse_xes(file)

    # If extension is csv
    elif file.filename.endswith('.csv'):
        file.save(os.path.join(path_data, 'raw.csv'))

    # Redirect to display
    return redirect('display', code=302)

@app.route('/display')
def display():
    """"
    Display raw data and preprocessed data
    """
    # TODO: pembacaan head nya sekali aja gausah berkali2
    # FIXME: cache masih jadi masalah buat file jsonnya
    # Convert raw csv file to json
    myjson = cv_json(raw_file, raw_json)

    # Header file raw
    list_raw_head = []
    for k in myjson[0].keys():
        list_raw_head.append(k)
    if os.path.isfile(final_file):
        # Convert final csv file to json
        myjson = cv_json(final_file, final_json)

        # Header file final
        list_final_head = []
        for k in myjson[0].keys():
            list_final_head.append(k)
    else:
        # Header reset
        list_final_head = 0

    # Render display.html
    return render_template('display.html', title='Display Data', raw=raw_json, final=final_json, raw_head=list_raw_head, final_head=list_final_head)

@app.route('/prepos', methods=['POST', 'GET'])
def prepos():
    """"
    Preprocessing raw data to be final data
    """
    # Check request method
    if request.method == 'GET':
        # Read raw file
        with open(raw_file) as f:
            reader = csv.reader(f)
            list_raw = list(reader)
            list_raw_head = list_raw[0]

        # Render prepos.html
        return render_template('prepos.html', head=list_raw_head, title='Preprocessing Data')

    # Choose which column to use (case id, task and timestamp)
    case_id = int(request.form['case_id'])
    task = int(request.form['task'])
    timestamp = int(request.form['timestamp'])

    # TODO: handle if there's missing value in the data
    # NOTE: SKIP for now
    # Missing value handling
    mis_val = 0

    # Data preprocessing
    preproccess(case_id, task, timestamp, mis_val)

    return redirect('filter', code=302)

@app.route('/filter', methods=['POST','GET'])
def filter():
    """"
    Filtering data based on column
    """
    # Check if file is already preprocessed or not
    if not os.path.isfile(final_file):
        return redirect('display', code=302)

    # Read raw file
    with open(raw_file) as f:
        reader = csv.reader(f)
        list_raw = list(reader)
        list_raw_head = list_raw[0]

    # Check request method
    if request.method == 'POST':
        # NOTE: Filtering can be done multiple times
        # TODO: Row filter based on which column and the criteria
        filter_col = 6
        criteria = 'rev'
        filter = [filter_col, criteria]
        filters()

    # Render filter.html
    return render_template('filter.html', title='Filtering Data', head=list_raw_head)

@app.route('/graph')
def graph():
    """"
    Displaying control flow based on the data
    """
    # Check if file is already preprocessed or not
    if not os.path.isfile(final_file):
        return redirect('display', code=302)

    # Convert csv to dictionary
    eLog = read_csv(final_file)
    # Count the transactions on the data using flow algorithm
    eLog = flow(eLog)

    # Determine the maximum and minimum value on the data
    max, min = find_max_min(eLog)

    # Render graph.html
    return render_template('graph.html', eLog=eLog, max=max, min=min)

@app.route('/delete')
def delete():
    """"
    Delete ALL the data
    """
    # Checking and deleting ALL the files
    if os.path.isfile(raw_file):
        os.remove(raw_file)
    if os.path.isfile(xes_file):
        os.remove(xes_file)
    if os.path.isfile(final_file):
        os.remove(final_file)
    if os.path.isfile(raw_json):
        os.remove(raw_json)
    if os.path.isfile(final_json):
        os.remove(final_json)

    # Redirect to index after deletion completed
    return redirect('/', code=302)

def find_max_min(eLog):
    """"
    Finding maximum and minimum value in the data
    """
    # Initialize max and min value
    max = 0
    min = float('inf')
    for task in eLog:
        for task2 in eLog[task]:
            val = eLog[task][task2]
            if max < val:
                max = val
            if min > val:
                min = val
    return max, min


if __name__ == '__main__':
    app.run(debug=True)
